<?php

class m130717_204706_create_table_tasks extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{tasks}}', array(
            'id' => 'pk',
            'taskTitle' => 'string NOT NULL',
            'taskDescription' => 'text',
            'creatorId' => 'integer NOT NULL',
            'assignedUserId' => 'integer',
            'taskStatus' => 'integer',
            'taskType' => 'integer',
            'timeSpend' => 'integer',
            'progress' => 'integer',
            'created' => 'integer',
            'updated' => 'integer',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{tasks}}');
	}
}