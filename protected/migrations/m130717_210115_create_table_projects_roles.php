<?php

class m130717_210115_create_table_projects_roles extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{projects_roles}}', array(
            'id' => 'pk',
            'projectRole' => 'string',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{projects_roles}}');
	}
}