<?php

class m130717_211142_create_table_attachments extends CDbMigration
{
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{attachments}}', array(
            'id' => 'pk',
            'path' => 'string',
            'attachmentName' => 'string',
            'ownerId' => 'integer',
            'type' => 'string',
            'created' => 'string',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{attachments}}');
	}
}