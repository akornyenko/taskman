<?php

class m130717_204719_create_table_projects extends CDbMigration
{
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{projects}}', array(
            'id' => 'pk',
            'projectTitle' => 'string NOT NULL',
            'projectDescription' => 'text',
            'projectStatus' => 'integer',
            'creatorId' => 'integer NOT NULL',
            'created' => 'integer',
            'updated' => 'integer',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{projects}}');
	}
}