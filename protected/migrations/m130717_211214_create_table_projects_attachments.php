<?php

class m130717_211214_create_table_projects_attachments extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{projects_attachments}}', array(
           'id' => 'pk', 
           'attachmendtId' => 'integer', 
           'projectId' => 'integer', 
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{projects_attachments}}');
	}
}