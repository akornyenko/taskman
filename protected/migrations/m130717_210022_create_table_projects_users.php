<?php

class m130717_210022_create_table_projects_users extends CDbMigration
{
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{projects_users}}', array(
            'id' => 'pk',
            'projectId' => 'integer',
            'userId' => 'integer',
            'projectRoleId' => 'integer',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{projects_users}}');
	}
}