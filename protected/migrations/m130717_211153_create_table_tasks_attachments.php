<?php

class m130717_211153_create_table_tasks_attachments extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{tasks_attachments}}', array(
           'id' => 'pk', 
           'attachmendtId' => 'integer', 
           'tasktId' => 'integer', 
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{tasks_attachments}}');
	}
}