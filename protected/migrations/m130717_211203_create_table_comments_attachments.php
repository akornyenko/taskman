<?php

class m130717_211203_create_table_comments_attachments extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{comments_attachments}}', array(
           'id' => 'pk', 
           'attachmendtId' => 'integer', 
           'commenttId' => 'integer', 
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{comments_attachments}}');
	}
}
