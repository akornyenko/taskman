<?php

class m130717_205959_create_table_roles extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{roles}}', array(
            'id' => 'pk',
            'role' => 'string',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{roles}}');
	}
}