<?php

class m130717_210842_create_table_tasks_comments extends CDbMigration
{
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{tasks_comments}}', array(
            'id' => 'pk',
            'taskId' => 'integer',
            'commenterId' => 'integer',
            'commentText' => 'text',
            'created' => 'integer',
            'updated' => 'integer',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{tasks_comments}}');
	}
}