<?php

class m130717_205944_create_table_users extends CDbMigration
{
	
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable('{{users}}', array(
            'id' => 'pk',
            'userName' => 'string',
            'email' => 'string',
            'password' => 'VARCHAR(40) NOT NULL',
            'displayName' => 'string',
            'avatar' => 'string',
            'roleId' => 'integer',
            'created' => 'integer',
            'updated' => 'integer',
        ));
	}

	public function safeDown()
	{
        $this->dropTable('{{users}}');
	}
}